<!DOCTYPE html>
    <?php
    // for auto refresh
    // $page = $_SERVER['PHP_SELF'];
    // $sec = "1";

    /* name of server on my computer
     * for others to access they can type my IP address into their browser
     * 192.168.178.79
     * or 10.....
    */
    $servername = "localhost";
    // for php mysql login
    $username = "root";
    $password = "";
    $dbname = "messyglossary";

    // create connection
    $conn = mysqli_connect($servername, $username, $password, $dbname);
    // check connection is valid
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

    // creating 2D array of words and definitions
    $table = array(
       array(
            "word"  => "calculator",
            "define" => "pretty",
        ),
        array(
            "word"  => "pen",
            "define" => "inky",
        ),
   
    );
    // print_r($table);

    // to access the data inside the keys we use a nested loop
    foreach ($table as $innerArray){
        // accessing each array within the main table array
        for($index = 0; $index < count($innerArray); $index++){   
            // inside each array contain the value in the keys 'word' and 'define'   
            $word = $innerArray["word"];
            $define = $innerArray["define"];
            // remember to specify each field
            // $sql = "INSERT INTO `glossarytable` (`id`, `word`, `define`, `imagelink`) VALUES (NULL, '$word', '$define', NULL)";
        }    
    }
   
    // checking INSERT INTO worked
    // if (mysqli_query($conn, $sql)) {
    //     echo "New record created successfully";
    // } else {
    //     echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    // }

    // to insert data from the html form into the php database
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // holding each input value in corresponding variable
        $word = $_REQUEST['word'];
        $definition = $_REQUEST['definition'];

        // inserting into table in database (remember to specify every header!)
        $sql = "INSERT INTO `glossarytable` (`id`, `word`, `define`, `imagelink`) VALUES (NULL, '$word', '$definition', NULL)";

        // checking the sql query worked with database 
        if (mysqli_query($conn, $sql)) {
            echo "new input submitted";
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($conn);
        }
    }
?>
    <head>
    <!-- for auto page refresh -->
    <!-- <meta http-equiv="refresh" content="<?php //echo $sec?>;URL='<?php //echo $page?>'"> -->
    <link rel="stylesheet" href="styles.css">
    </head>

    <body> 
        <div class="sidenav">      
            <?php
            // if you keep re running this you add duplicates to the db
                // querying the current database
                $query = "SELECT * FROM glossarytable";
                $result = mysqli_query($conn, $query);

                // to store the previous row's word
                $previousWord = '';

                // fetching the data
                while($row = mysqli_fetch_assoc($result)) {
                    // check if the current word is different from the previous word
                    if($row['word'] != $previousWord) {
                        // store the current word as the previous word for the next iteration
                        $previousWord = $row['word'];

                        // formatting into HTML for sidenav
                        echo "<a href='#' id='word'>" . $row['word'] . "</a>";
                    }
                }

                mysqli_close($conn);
            ?>

        <h2>new entry</h2>
            <form action="" method="post">
                <div class="container">
                        <label for="word"> word </label>
                        <input type="text" id="word" name="word" value="">
                </div>

                <div class="container">
                        <label for="definition"> definition </label>
                        <input type="text" id="definition" name="definition" value="">
                </div>
                <button type="submit">submit</button>
            </form>
        </div>
        <!-- <div id="main"> -->

        
        <!-- </div> -->

    </body>

    <!-- <script>
        // removes current URL from browser history so user can't go back
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
    </script> -->
    
</html>

